from flask import *
# class Flask
from flask_sqlalchemy import SQLAlchemy
import json


# เรียกคลาส Flask มาเก็บไว้ในตัวแปร app
app = Flask(__name__)
db = SQLAlchemy(app)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///demo.db'

class User(db.Model):

    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(60), unique=True, nullable=False)
    email = db.Column(db.String(60), unique=True, nullable=False)

    def json(self):
        return {
            'id': self.id,
            'username': self.username,
            'email': self.email
        }
    
    # create 
    def add_user(_username, _email):
        new_user = User(username=_username, email=_email)
        db.session.add(new_user)
        db.session.commit()
    
    # get all data
    def get_all_user():
        return [User.json(user) for user in User.query.all()]
    
    # get data by id
    def get_user_by_id(_id):
        return [User.json(User.query.filter_by(id=_id).first())]

    # update user
    def update_user(_id, _username, _email):
        user_to_update = User.query.filter_by(id=_id).first()
        user_to_update.username = _username
        user_to_update.email = _email
        db.session.commit()
    
    # delete user
    def delete_user(_id):
        # filter by id and delete
        User.query.filter_by(id=_id).delete()
        db.session.commit()


# route path คือ url 
@app.route('/')
#function ใน route path
def home():
    # 
    return "Hello, world!!"

@app.route('/home')
def code():

    return "<h1> Hello </h1>"

@app.route('/gogo')
def gogo():
    return render_template("index.html")


@app.route('/user/all', methods=['GET'])
def get_user():
    # print(User.get_all_user())
    return jsonify(User.get_all_user())

@app.route('/user/<int:id>', methods=['GET'])
def get_user_by_id(id):
    try:
        return_value = User.get_user_by_id(id)
        print(return_value)
        return jsonify(return_value)
    except:
        return render_template("index.html")

@app.route('/user', methods=['POST'])
def create_user():
    request_data = request.get_json()
    User.add_user(request_data["username"], request_data["email"])
    response = Response("User added", 201, mimetype="application/json")
    print(response)
    return response

@app.route('/user/<int:id>', methods=['PUT'])
def update_user(id):
    request_data = request.get_json()
    User.update_user(id, 
                    request_data['username'],
                    request_data['email'])
    response = Response("User Updated", status = 200, mimetype='application/json')
    return response


@app.route('/user/<int:id>', methods=['DELETE'])
def remove_blog(id):
    User.delete_user(id)
    response = Response("User Deleted", status = 200, mimetype='application/json')
    print(response)
    return response

if __name__ == "__main__":
    app.run(port=5678 ,debug=True)



    methods = ["GET", "POST", "PUT", "DELETE"]

    # GET = 1,2,3,4,5
    # POST = post ค่าออกไป
    #  PUT = เปลี่ยนแปลงค่าใน database
    # DELETE = ลบค่าจาก database